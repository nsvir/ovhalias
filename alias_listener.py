from apscheduler.schedulers.background import BackgroundScheduler
import time

from send_mail import send_confirmation

def listen_alias(alias, status_fetcher):
    def currified():
        while status_fetcher() != "ok":
            time.sleep(0.5)
        time.sleep(2) # extra waiting time to ensure validation is done
        tries=3
        while send_confirmation(alias) == False and tries > 0:
            time.sleep(2)
            tries -= 1
        print("{} is available!".format(alias))
        
    return currified

class AliasListener:
    scheduler = BackgroundScheduler()

    def __init__(self):
        self.scheduler.start()

    def listen(self, alias, status_fetcher):
        self.scheduler.add_job(listen_alias(alias, status_fetcher))