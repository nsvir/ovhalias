import ovh

from ovh import NotCredential, InvalidKey, InvalidCredential


class OvhAlias:
    # create a client using configuration
    client = ovh.Client(config_file="conf/ovhalias.conf")
    alias_url = "/email/exchange/hosted-sn249761-1/service/hosted-sn249761-1/account/nicolas@nsvir.fr/alias"
    granted_url = "/email/exchange/*"
    rules = [
        {'method': 'GET', 'path': granted_url},
        {'method': 'POST', 'path': granted_url},
        {'method': 'DELETE', 'path': granted_url}
    ]
    cached_status={}

    def login(self, redirect): 
        print("Redirecting: " + redirect)
        ck = self.client.request_consumerkey(access_rules=self.rules, redirect_url=redirect)
        return (ck["validationUrl"], ck["consumerKey"])

    def is_logged_out(self, ck):
        try:
            self.query(ck, lambda c: c.get(self.alias_url))
            return False
        except InvalidKey as error:
            print("Logged-out: " + str(error))
        except NotCredential as error:
            print("Logged-out: " + str(error))
        except InvalidCredential as error:
            print("Logged-out: " + str(error))
        return True

    def alias_status(self, ck):
        def with_ck(alias):
            if (alias not in self.cached_status or self.cached_status[alias] != 0):
                print("Fetching: {}".format(alias))
                self.cached_status[alias] = self.query(ck, lambda c: c.get("{}/{}".format(self.alias_url, alias)))["taskPendingId"]
            return {"alias": alias.split("@", 1)[0], "status": "ok" if self.cached_status[alias] == 0 else "pending" }
        return with_ck

    def status(self, ck, alias):
        return "ok" if self.query(ck, lambda c: c.get("{}/{}".format(self.alias_url, alias)))["taskPendingId"] == 0 else "pending"

    def query(self, ck, call):
        self.client._consumer_key = ck
        result = call(self.client)
        self.client._consumer_key = None
        return result

    def list(self, ck):
        aliases = self.query(ck, lambda c: c.get(self.alias_url))
        enriched = map(self.alias_status(ck), aliases)
        return sorted(list(enriched), key=lambda x: x["alias"])

    def delete(self, ck, alias):
        del self.cached_status[alias]
        return self.query(ck, lambda c: c.delete("{}/{}".format(self.alias_url, alias)))

    def add(self, ck, alias):
        return self.query(ck, lambda c: c.post(self.alias_url, alias=alias))