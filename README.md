OvhAlias
========

# Use
You need python 3.9.2
Install `pip install -r requirements.txt`
You need a valid `conf/ovhalias.conf`
then run `python main.py`


# Build
```sh
docker build -f Dockerfile -t ovhalias .
docker run -p 8080:8080 -v $(pwd)/conf:/srv/conf:ro -e TZ="Europe/Paris" ovhalias
```

# Deploy
```sh
wget https://gitlab.com/nsvir/ovhalias/-/archive/main/ovhalias-main.tar.gz
tar xvfz ovhalias-main.tar.gz
rm ovhalias-main.tar.gz
cp ovhalias-main/conf .
<edit conf/ovhalias.conf>
```