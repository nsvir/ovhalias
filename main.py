
import functools
import time

from itertools import groupby
from bottle import jinja2_view, run, post, get, redirect, request, response
from alias_listener import AliasListener

from ovhalias import OvhAlias
view = functools.partial(jinja2_view, template_lookup=["templates"])

ovhalias = OvhAlias()
aliaslistener = AliasListener()

def minutes():
    return int((time.time() / 60) - 27386570)

def consumer_key():
    ck = request.get_cookie('consumer_key')
    if (ovhalias.is_logged_out(ck)):
        validationUrl, ck = ovhalias.login(request.url)
        response.set_cookie('consumer_key', ck)
        redirect(validationUrl)
    return ck

def is_pending(aliases):
    try:
        return next(filter(lambda x: x["status"] == "pending", aliases)) != None
    except StopIteration:
        return False


@get("/")
@get("/new")
@get("/delete/<alias>")
def index():
    redirect("/")

@get("/")
@view("alias.html")
def aliases():
    ck = consumer_key()
    aliases = ovhalias.list(ck)
    grouped_alias = groupby(aliases, lambda x: x["alias"][0])
    return { "aliases": grouped_alias, "nextTmp": "temp{}".format(minutes()), "autorefresh": 15 if is_pending(aliases) else 0 }

@post("/delete/<alias>")
def delete_alias(alias):
    ck = consumer_key()
    delete_alias = "{}@nsvir.fr".format(alias)
    print("Deleting: ", delete_alias)
    print(ovhalias.delete(ck, delete_alias))
    redirect("/#{}".format(alias[0]))

@post("/new")
def add_alias():
    ck = consumer_key()
    new_alias = "{}@nsvir.fr".format(request.forms.get("new_alias"))
    print("Adding: ", new_alias)
    print(ovhalias.add(ck, new_alias))
    aliaslistener.listen(new_alias, lambda: ovhalias.status(ck, new_alias))
    redirect("/#{}".format(new_alias[0]))

run(host='0.0.0.0', port=8080)