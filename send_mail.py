import smtplib, ssl, configparser
from smtplib import SMTPDataError

config = configparser.ConfigParser()
config = configparser.ConfigParser()
config.read('conf/ovhalias.conf')
smtp_conf = config['smtp']

port = smtp_conf["port"]
smtp_server = smtp_conf["host"]
password = smtp_conf["password"]
username = smtp_conf["username"]

def message(alias):
    return """\
Subject: {0} is available !

Your alias: {0}

https://alias.nsvir.fr
Enjoy ! ❤️""".format(alias)

def send_confirmation(alias):
    context = ssl.create_default_context()

    with smtplib.SMTP(smtp_server, port) as server:
        server.starttls(context=context)
        server.login(username, password)
        try:
            server.sendmail(alias, alias, message(alias).encode("utf-8"))
            return True
        except SMTPDataError as error:
            print(str(error))
            return False
