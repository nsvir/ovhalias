FROM python:3.9.2-alpine

WORKDIR /srv/
COPY requirements.txt .
RUN pip install -r requirements.txt

COPY *.py ./
COPY templates templates

ENV PYTHONUNBUFFERED 1


RUN apk add tzdata
ENV TZ Europe/Paris
RUN cp /usr/share/zoneinfo/Europe/Paris /etc/localtime
RUN echo "Europe/Paris" >  /etc/timezone
RUN date

EXPOSE 8080
CMD ["python", "main.py"]